# Domain name services

## Présentation

Le service DNS offre un système de nommage des services internet. Il permet de parler d'un service internet, comme un site web, par son nom.

Il a été créé à la demande de la DARPA, par Jon Postel et Paul Mockapetris en 1983 (RFC882 et 883). Ce système de nommage vient remplacer un fichier `hosts` partagé entre les serveurs de l'internet (5500 hosts en 1987 dans le fichier hosts contre 20 000 hosts dans le service DNS)

C'est une arborescences de nom avec délégation d'autorité sur des sous arborescences. __C'est un système de nommage distribué__

N'hésitez pas à lire les pages Wikipédia : DNS, Serveur racine du DNS, TLD etc...

### Organisation technique

Les 13 serveurs racines gérés par l'ICANN (société de droit californien de régulation de l'internet) font autorité sur l'ensemble des noms DNS, il délègue cette authorité aux serveurs TLD (Top Level Domaine)

Les TLD font autorité sur le dernier secteur de nommage des noms DNS. 
Il y a : 

- les nationaux (.fr, .ch, .be ~260),
- les génériques (.net, .org ~20),
- les nouveaux (.job, .live, .beer ~n), 
- le spécial (.arpa =1). 

Ils sont gérés par des organisations indépendantes (Exemple AFNIC pour .fr)

Nous déposons en général des domaines de second niveaux (wikipedia.org) pour ce faire nous obtenons la délégation de l'autorité sur ce secteur du système de nommage auprès du TLD (.org).

### La résolution DNS

C'est le fait de retrouver l'adresse de service internet à partir de son nom.

Exemple : www.framasoft.org

__Principe :__
Un poste client interroge son serveur DNS de récursion. Par exemple celui de la box.

- ce dernier pour résoudre le nom va interroger les 13 serveurs racines,
- qui le renvoit vers les serveurs du TLD associé à la recherche qui fait autorité (.org),
- ce dernier le renvoit vers les serveurs qui hébergent le nom de domaine (framasoft.org) qui font donc autorité sur ce domaine
- et qui enfin retourne l'adresse attendu pour www.framasoft.org.

![](./images/dns-recurse.png)

En ligne de commande vous pourrez utiliser la commande `dig`.

usage :

```bash
dig [record-type] domain-name [@server-dns-ip]
dig -x ip-address [@server-dns-ip]
man dig
```

exemple :

```bash
$ dig fr.wikipedia.com @8.8.8.8 +short
ncredir-lb.wikimedia.org.
208.80.154.232
```

Cette commande permet d'interroger manuellement le service distribué DNS en choissisant explicitement avec quel serveur de récursion (ici @8.8.8.8) la requête est effectuée.

> Sous GNU/linux la résolution est en général effectué via le fichier /etc/hosts puis par interrogation des serveurs dns de recursion configurée dans le fichier `resolv.conf`. voir : [resolveur DNS](./using-dns.md#Le-resolver-DNS)

### Le reverse dns

Le domaine in-addr.arpa est dédié à l'opération inverse : à partir d'une adresse ip retrouver un nom.

Il est organisé en ordre inverse des adresses ip :

```bash
$ dig -x 208.80.154.232 +short
ncredir-lb.eqiad.wikimedia.org.
$ dig ptr 232.154.80.208.in-addr.arpa +short
ncredir-lb.eqiad.wikimedia.org.
```

L'adresse ip 208.80.154.232 est référencé par le domaine 232.154.80.208.in-addr.arpa. L'ordre des nombres est inversés car le subnetting est fait de gauche à droite alors que les noms de domaine sont délégués de droite à gauche.

L'autorité est alors delégué par subnet (Les AS du net).

### Les enregistrements d'un nom de domaine

Un nom de domaine fait autorité sur la "zone" associé à celui-ci : il contient des enregistrements de noms.

ceux ci sont de la forme suivante :

NOM   TTL   CLASSE  TYPE  DATA

- Nom : le nom, exemple "www"
- TTL : le time to live : la durée de vie en cache de l'enregistrement (les récurseur conserve en cache les résultats obtenus pendant la durée du TTL)
- Class : IN pour Internet Name 
- Type : les types d'enregistrements décrit plus bas
- data : le contenu de l'enregistrement

__Les Types d'enregistrement :__

- A  : associent une IP à un nom (peuvent être des glue records)
- CNAME : associent un autre nom à un nom (alias)
- AAAA : associent une IPV6 a un nom
- TXT : un texte, plusieurs usages possible permet de valider la détention d'un domaine
- PTR : spécifique au reverse DNS renvoit le nom pour l'IP sous domaine de in-addr.arpa
- MX : définit les serveurs de mails associés à la zone avec leur priorité d'usage
- NS : Name serveur : il définit les serveurs de noms faisant autorité et permet de déléguer l'autorité d'une sous zone
- SOA : Start of authority c'est le début de l'enregistrement d'un domaine il définit le domaine lui même et comment il est hébergé.

### Types de serveur de noms

Sur ce système de nommage les serveurs de noms de l'internet collaborent à la résolution de noms.

chacun des serveurs peu founir les fonctionnalités suivantes :

- authoritatif : le serveur répond car il fait autorité sur le domaine
- recursion : le serveur va rechercher et récuperer le résultat de la requête DNS
- Forward : le serveur dns transmet la requête vers un autre serveur de noms
- caching : le serveur conserve en cache les données pendant la durée du TTL

![](./images/dns-full.png)

Je vous propose un [TD](./TD-dns.md) qui vous permettra d'y voir un peu plus clair sur les serveur recursifs et authoritatifs.

## Organisation Administrative

Chaque TLD réglemente la gestion de domaines de second niveau. Exemple en Chine, il faut déclarer une personne physique résidant en Chine en référent pour chaque domaine.

### Présentation

__la racine :__ Les registres des 13 serveurs racines référencent l'ensemble des TLD, il sont gérés par l'ICANN. Les serveurs eux sont gérés par l'IANA (Internet Assigned Numbers Authority). La création d'un nouveau TLD se fait donc auprès de l'ICANN en respectant une certaine réglementation.

__Les registres des TLD :__ Les registres des TLD sont maitenus par les Network Information Center (NIC). Ils référencent tous les noms de domaines de niveau inférieur. Exemple l'afnic pour le domaine .fr référence tous les domaines qui finissent par .fr. Les nic maintiennent les base de donnée whois consultable sous conditions (suivant les cas avec la commande whois).

__Les bureau d'enregistrement :__ Ou registraire de nom de domaine ; **registrar** en anglais. Ce sont des entités en générale privée qui ont obtenues le droits de passer des commandes aux gestionaire de registre et vendent donc ce service à des clients finaux. Chaque gestionaire de registre definit ses conditions ; le registrar doit donc démarcher tout les gestionaires de registres pour lesquel il souhaite "vendre" des noms de domaine. ovh, gandi, etc...

__L'hébergeur de noms de domaine :__ c'est l'entité qui administre les serveurs authoritatifs associé au domaine.

### Opérations sur les noms de domaine

__Mise à jour des enregistrements :__
les simples : ajout, suppression, modification des enregistrements sont effectués auprès de l'hébergeur de noms de domaine qui founit en général une interface web pour le faire.

__Déclaration :__

Un domaine est donc déclaré par un registrar (registraire) qui dépose pour une période donnée au niveau du registre :

- Le nom de domaine
- Les noms et ip des serveurs faisant autorités pour ce domaine
- Le contact "owner" le dépositaire (entreprise personne ou association)
- Le contact registrar (lui même)
- Un contact administratif
- Un contact technique

Le client, l'utilisateur, fait donc appel à un registrar pour obtenir ou déclarer un nom de domaine

__Renouvèlement :__

De façon périodique, le domaine doit être renouvelé dans le cas contraire il peut être perdu, ou récupèrer par une autre entitée.

__Migration de registrar :__

Afin de changer de prestataire registrar, il est possible d'effectuer une migration de registrar communément appelé migration de domaine.

On récupère auprès du registrar actuel le `authcode` associé au domaine, on déclare la migration auprès du nouveau registrar en lui founissant le code. Le nouveau registrar peut alors modifier le registre en se déclarant comme interlocuteur registrar sur le domaine.

> __Attention :__ dans la pratique le registrar est souvent l'hébergeur du nom de domaine (celui qui gère les serveurs authoriotatif lié au domaine). Donc la migration concerne aussi l'hébergement du domaine. Il convient donc avant de migrer de récupérer tous les enregistrements et de les déclarer sur les serveurs du nouvel hébergeur

__Cycle de vie du domaine :__

La déclaration du nom de domaine ne peut pas être modifier fréquemment chaque modification tel le renouvellement ou le changement de registrar est associé à une période de gel qui suit la modification.
![](./images/domain-life-cycle.png)

__Cas du nom renouvèlement :__

Le nom de domaine entre dans une période de gel durant laquel il est possible de restaurer le nom de domaine parfois suivis d'un période de quarantaine avant que le domaine soit de nouveau disponible pour être re-déclaré.

> Attention la restoration d'un nom de domaine est beaucoup plus coûteuse qu'un renouvellement.
