## Bonjour !

### Un petit tuto pour utiliser molecule et un environnement virtuel.

#### Qu'est ce que c'est ?

> Un virtualenv permet d’avoir des installations de Python isolées de l’OS, et séparées les unes des autres pour chaque projet.

- Installation de `virtualenv` :  
  (Il se peux que vous l'ayez déjà installé)
  - `pip3 install --user virtualenv`

* Utilisation :
  - `virtualenv /path/ou/vous/voulez/nom_du_venv`
  - Soit `virtualenv /path/ou/vous/voulez/molecule_env`

Virtualenv va créer un dossier avec un environnement complet dedans: l’interpreteur Python, les libs, des commandes, etc.  
C’est votre installation isolée.

- Activer votre virtualenv pour travailler dedans:
  - Sous MacOS, et Linux :  
    `source /path/vers/projet/env_nom_du_projet/bin/activate`
  - Sous Winbouze :  
    `C:\\path\vers\projet\env_nom_du_projet\Scripts\activate.bat`

Une fois votre environnement activé vous pouvez remarquer que le prompt de votre terminal prend le nom entre parenthèse de l'environnement.

- Install de molecule 2.22 dans le venv :
  - `pip3 install molecule==2.22`

Vous pouvez biensur installer autant de bibliothèques python que vous voulez à l'intérieur.

A partir de là vous pouvez mener à bien le [tuto molécule de Alsim](https://gitlab.com/alsim/linux1/-/blob/master/tuto-molecule-fr.md).

- Une fois le travail éffectué vous pouvez quitter le `venv` en faisant :
  - `deactivate`

> Un super lien pour bien comprendre les virtualenv : http://sametmax.com/les-environnement-virtuels-python-virtualenv-et-virtualenvwrapper/
