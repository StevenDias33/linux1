# Cours GNU/linux

Je reconstruis mes cours GNU/Linux sur la base d'un dépôt git en markdown. Ils sont disponibles dans ce dépot sous [licence Creative Commons](./LICENCES.md) CC-by-nc-sa 4.0  (c)Alan Simon *alsim*

Ils sont bien sur destinés aux débutants toutes les remarques sont bienvenues.

## Chapitre 1, Installation et utilisation

### Introduction

* Quelques éléments de contexte [historique](./historique.md) d'Unix et Linux
* Une [présentation](./presentation_Linux.md) des distribution GNU/Linux
* Une [première installation](./TD-install-ubuntu.md) sous virtualbox d'une Ubuntu

### Utilisation du shell

* Une présentation du [shell](./shell.md)
* Le [man](./man.md)
* Les principales [commandes](./commands.md) et un [TD](./TD-commands.md)
* L'[arborescence](./arborescence.md) de fichiers
* Les commandes [filtres](./filtres.md) et encore un [TD](./TD-filtres.md)
* L'[environnement](./environment-Unix.md) d'execution du shell et un [TD](./TD-environment.md)
* La gestion des [processus](./processus.md)
* Comment [rechercher](./find.md) des fichiers dans une arborescence avec la commande `find`

### Scripting

* [scripting bash](./scripting.md) et son [TD](./TD-scripting.md)

### Outils à connaitre

* Le client [ssh](./ssh-client.md)  
* Edition de fichier avec [vi et sed](./vi.md)  
* Le tableur en ligne de commande : [awk](./awk.md)  
* L'outil de synchronisation [rsync](./rsync.md)  
* La planification des taches: [cron](./cron.md)

## Chapitre 2, administration serveur

### Les utilisateurs et les droits

* Gestion des comptes [utilisateurs](./utilisateurs.md) et un [TD](./TD-utilisateurs.md)
* Gestion des [Droits](./droits.md) sur les fichiers et encore un [TD](./TD-droits.md)

### Installation / Configuration

* [Installation et configuration d'une centOS 7](./TD-install-centos.md)
* [Configuration DNS](./using-dns.md)
* présentation des [bibiothèque partagés](./shared-libs.md)
* [gestion de package logiciel](./packages.md) et un [TD](./TD-install-soft.md) sur les installation logiciels
* [gestion des mails](./using-mail.md) et un [TD](./TD-mail-system.md)

### Gestion du stockage

* Cours sur les [Les filesystèmes](./filesystem.md) et un [TD](./TD-filesystem.md) sur le sujet
* Cours sur le Logical Volume Manager [LVM](./lvm.md) et un [TD](./TD-lvm.md) sur le sujet

### Troubleshooting

* [Principe du troubleshooting](./troubleshoot.md)
* La [Syslog](./syslog.md) et un [TD](./TD-syslog.md)
* Comprendre le [Boot](./boot.md)

## Chapitre 3, les services réseaux sous linux

### Préambule

* Présentation [vagrant](./vagrant.md) et son [TD](./TD-vagrant.md)
* Mise en oeuvre [vagrant sous windows avec wsl](./procedure-vagrantWSL.md)
* [TP en autonomie](TP-vagrant.md)
* Notion de socket [socket](socket.md)
* [SSL/TLS](SSL.md)

### Les services http

* Le protocole [http](http.md)
* apache httpd server [apache](apache.md) et son [TD](TD-apache.md)
* [TP en autonomie](TP-http.md)

### Le service DNS

* Domain Name Services [DNS](./dns.md) et un [TD](./TD-dns.md)

### Les containers

* Présentation du [principe de contenerisation](./contener_principes.md)
* [Utilisation docker](./utilisation_docker.md)
* Gestion des [images docker](images_docker.md)
* intégration d'un [service conteneurisé](service_docker.md) sur un host.
* [TP](./TP-docker-gitea.md) : Contruire une image gittea
* [docker-compose](docker-compose.md)

### La gestion de conf avec Ansible

* Présentation de [la gestion des configurations](./config-mgt.md)
* [Tutoriel découverte d'Ansible](./tuto-ansible-fr.md)
* mise en oeuvre [docker sous windows avec wsl](./procedure-dockerWSL.md)
* tester ses roles avec [molecule](./tuto-molecule-fr.md)
* [Intégration Ansible dans Vagrant](./vagrantsible.md)
* [Discussion](./how-to-manage-Ansible.md) autour de l'organisation de la gestion des configurations avec ansible
