# Intégration ansible dans Vagrant

## Presentation

Vagrant intègre complètement ansible en tant que type de provisionneur il suffit pour cela de placer dans le dossier projet vagrant une arborescence Ansible minimal :

* les playbooks
* les dossiers roles avec les roles

L'avantage est que :

* Vagrant va pouvoir gérer l'inventaire pour nous (il connais déja les hosts)
* le compte utilisateur vagrant est sufisant, pas besoin de créer un compte et de déposer la clef

Bref, c'est clef en main.

## Utilisation

Voici un exemple de provision ansible pour le playbook `site.yml` en utilisant le compte vagrant (force_remote_user=true):

```ruby
  config.vm.provision "ansible" do |ansible|
    ansible.playbook = "site.yml"
    ansible.force_remote_user = true
  end
```

Vous pouvez aussi grouper vos hosts et y ajouter des variables d'inventaire (à l'intérieur de la définition du provisionning):

```ruby
ansible.groups = {
  "bordeaux" => ["host1", "host2"],
  "bordeaux:vars" => {"ntp_server" => "ntp.emi.u-bordeaux.fr",
                     "proxy" => "proxy.lab.local",
                     "http_port" => 443}
}
```

ou simplement surcharger des variables au niveau des hosts:

```ruby
ansible.host_vars = {
  "host1" => {"http_port" => 80,
              "maxRequestsPerChild" => 100},
  "host2" => {"http_port" => 8080,
              "maxRequestsPerChild" => 200}
}
```

> regardez [plutôt](https://tse1.mm.bing.net/th?id=OIP.Wnf09pK2VfkHGyDAuopOMQHaHa&pid=Api&P=0&w=300&h=300) la [documentation officielle](https://www.vagrantup.com/docs/provisioning/ansible.html) pour voir toute les options possibles.

__Attention,__ le provisionning vagrant si défini sur tout les hosts sera exécuté sur chaqun des hosts indépendament. Or les playbook précisent déja sur quel hosts ils doivent s'appliquer. Il est préférable de définir le provisioning ansible **sur le dernier host** uniquement puis de preciser qu'il s'applique à tous les hosts (limit="all"):

```ruby
ansible.limit = "all"
```

Usage intéressant:

* Vous travaillez sur un projet open source de dev ou de configuration système dans un dépot git.
* Vous créez un dossier vagrant à la racine de ce projet. dans ce dossier, vous codez le vagrantfile et le code ansible permettant de construire un environnement de test

Les contributeur du projet peuvent alors diposer d'un environnement local de test simplement, la contribution est alors beaucoup plus simple

Les éventuel utilisateur du projet peu aussi disposer d'un environement dévaluation très simplement.

__A faire (utilise ansible dans vagrant):__

Vous crérez un `Vagrantfile` et utiliserez le rôle du tutoriel Ansible afin d'utiliser l'intégration d'ansible dans vagrant.

__A faire (installer netbox):__

liser le readme, et installez cette application : netbox
avec le vagrantfile qui déploi un hosts de test et le provisionne via ansible. ce playbook déplois netbox et ses dépendance. Vous devrez donc installer deux autre rôles avec ansible-galaxy sur votre environnement.
