# Le protocole http

Voici une brève présentation du protocole, le minimum à connaître. 
Vous pouvez vous réfèrez wikipédia : https://fr.wikipedia.org/wiki/Hypertext_Transfer_Protocol qui vous renvera vers les RFC corespondantes

## Principe du web

### L'URL

Pour Uniform Resource Locator , l'URL designe une ressource localisée. cf : [RFC 3986](https://tools.ietf.org/html/rfc3986)

Exemple, ce cours est situé sur l'url : `https://gitlab.com/alsim/Linux1`

Format :

`protocole://[user[:password]@]server.domaine[:port]/[web-route/][page]`

- protocole : le protocole de communication : http, https, ftp etc...
- user : l'éventuel compte utilisateur
- password : l'éventuel mot de passe
- serveur.domain : le host qui héberge la ressource
- port : le port TCP du service réseau qui héberge la ressource si ce n'est pas le port par défaut du protocole
- web-route : le chemin d'accès à la ressource au travers de ce protocole sur ce serveur.
- page : le nom de la ressource.

### Fonctionnement

- L'utilisateur saisie une URL sur son navigateur

- Le navigateur effectue une résolution DNS du nom de domaine associé au serveur défini dans l'url via le [resolveur DNS](./using-dns.md#Le-resolver-DNS)

- Le navigateur ouvre alors une connexion tcp vers l'ip retourner et sur le port relatif à l'url (http : 80, https : 443, ftp : 21 ou le port spécifier après le serveur name)

- Le navigateur effectue alors une requête http vers le serveur web au travers de la connexion TCP.

- Le serveur web retourne le contenue de la page web demandée

## le protocole http

Le protocole HTTP est une norme de l'IETF (Internet Engineering Task Force) :
- HTTP 1.0 est décrit dans la [RFC 1945](https://tools.ietf.org/html/rfc1945)
- HTTP 1.1 est décrit par la [RFC 2616](https://tools.ietf.org/html/rfc2616) et précisée dans les RFC 7230-7237

C'est la couche applicative HTTP traité sur une couche transport (en général TCP/IP). Ce protocole fonctionne sous la forme de requête/réponse

### formats

__format d'une requête :__

```xml
<methode> <web-route><page> <version du protocole>
<entête de requête>
[ligne vide]
<corps de requête>
[ligne vide]
```

__Format d'une réponse :__

```xml
<version du protocole> <code de réponse> <texte réponse>
<entête de réponse>
[ligne vide]
<corps de réponse>
[ligne vide]
```

__Métodes :__

- GET : récupération d'une page
- HEAD : récupération des entêtes de réponse uniquement
- POST : récupération d'une page avec transmission de données dans la requêtes
- PUT : upload de fichiers sur le serveur
- DELETE : suppression de fichier sur le serveur
- CONNECT : requête de connexion https

__Codes retours :__

- 100 – 199 : information
- 200 – 299 : code retour sur succes (Exemple : 200 OK)
- 300 – 399 : erreur corrigeable par le navigateur (exemple : 301 redirection permanente)
- 400 – 499 : erreur dand la requête (Exemple : 404 page not found)
- 500 – 599 : erreur coté serveur (Exemple : 500 Internal server error)

__Exemple de communication HTTP :__

On utilise ici l'outil `telnet` qui lance une session de communication interactive sur un port TCP distant.
On saisi la requête http `GET / HTTP/1.0` puis deux ligne vide.
le serveur web distant (nginx) retourne sa réponse avec le code retour 301

```bash
$ telnet kernel.org 80
Trying 198.145.29.83...
Connected to kernel.org.
Escape character is '^]'.
GET / HTTP/1.0

HTTP/1.1 301 Moved Permanently
Server: nginx
Date: Thu, 03 Oct 2019 13:16:19 GMT
Content-Type: text/html
Content-Length: 178
Connection: close
Location: https://_/
X-Frame-Options: SAMEORIGIN

<html>
<head><title>301 Moved Permanently</title></head>
<body bgcolor="white">
<center><h1>301 Moved Permanently</h1></center>
<hr><center>nginx</center>
</body>
</html>
Connection closed by foreign host.
$
```
